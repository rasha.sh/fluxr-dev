fluxr is a python 3 based operator of xflux. 

The purpose of this project is to create a streamline user experience of
xflux. 

As it stands now, xflux needs to be invokes manually with GPS coordinates and
killed manually by typing in the kill command. It's not very difficult to
perform these tasks manually. However, the complexity of operation goes up if
more locations are to be used (try remembering all these GPS coordinates).
Another aspect I want to address is that xflux seems to be happy running many
concurrent processes essentially running the same code. 

fluxr is personal tool I intend to develop to help address the above issues. 

Goals - 

- Streamline xflux operations thorough one program.
- Check if xflux is running - fluxr status - Done.
- start fluxr with default coordinates - fluxr -Done.
- kill the xflux process - fluxr stop -Done.
- Settings functionality.
- Coordinates selection fluidity.

Active features - 
- Run xflux with coordinates.
- Check status
- Kill xflux processes.


#!/usr/bin/env python3.7
# A script to operate xflux daemon
#TODO: Implement settings feature.
#TODO: make a gui frontend.


import subprocess
import argparse
import sys
import psutil

class Fluxr(object):
    """
    xflux operator.

    main methods: runxflux(), checkstatus(), stopxflux()

    """
    def __init__(self):
        pass

    def glenroy(self):
        return 37.7046, 144.9173
        

    def melb_city(self):
        return 37.82, 144.97

    def runxflux(self, l, g):
        """
        This function runs the xflux daemon.
        """
        # Check to see if another instance of xflux is running.
        if len(self.checkstatus()) == 0:
            subprocess.run(['xflux', '-l', str(l), '-g', str(g)])
        else:
            print(f'xflux is already running. \nPID: {self.checkstatus()}')

    def checkstatus(self):
        """
        This function returns a list of xflux PIDs else prints out that no
        processes were found.
        """
        proc_list = []
        for proc in psutil.process_iter():
            if 'xflux' in str(proc.name):
                proc_list.append(proc.pid)
        return proc_list

    def stopxflux(self):
        """
        This function queries checkstatus() to get a list of all the running
        xflux processes and kills them. It doesn't return anything.
        """
        kill_list = self.checkstatus()
        if kill_list:
            for proc in psutil.process_iter():
                if proc.pid in kill_list:
                    try:
                        print(f'Killing process -> {proc.pid}')
                        proc.kill()
                    except:
                        print('Error: Can not kill xflux PID.')
        sys.exit(0)
    

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='xflux initiator\nDefault is Glenroy.')
    parser.add_argument('-m', action='store_true', help='Starts xflux with Melbourne City Coordinates.')
    parser.add_argument('-stop', action='store_true', help='Stops the running xflux process (if any).')
    parser.add_argument('-status', action='store_true', help='Checks is xflux\
            is already running.')
    args = parser.parse_args()
    
    # Initialising the Fluxr class
    a = Fluxr()

    # Going through the arguments. If not argument is provided fluxr runs the
    # default coordinates at Glenroy. TODO: Settings (file and implementation)
    if args.status:
        if a.checkstatus():
            # If there are xflux processes running they are displayed as a
            # list.
            print(f'xflux PIDs {a.checkstatus()}') 
        sys.exit(0)
    elif args.m:
        l, g = a.melb_city()
    elif args.stop:
        # Stops any xflux processes.
        a.stopxflux()
    else:
        l, g = a.glenroy()

    # Running xflux
    a.runxflux(l, g)
